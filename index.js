var koa = require('koa')
var app = koa()
var MongoClient = require('mongodb').MongoClient
var assert = require('assert')

// Connection URL
var url = process.env.MONGO
// Use connect method to connect to the Server
MongoClient.connect(url, function (err, db) {
  assert.equal(null, err)
  console.log('Connected correctly to mongo')

  app.use(function * () {
    this.body = yield new Promise((resolve, reject) => {
      db.collection('users').find({}).toArray((err, results) => {
        if (err) {
          reject(err)
        }

        resolve(results)
      })
    })
  })

  app.listen(3000)
})
